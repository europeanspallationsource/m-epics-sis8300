/**
 * Struck 8300 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300AIChannelGroup.cpp
 * @brief Implementation of sis8300 card analog input channel group in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include <math.h>

#include "ndsChannelGroup.h"
#include "ndsChannelStateMachine.h"
#include "ndsTrigger.h"
#include "ndsTriggerCondition.h"
#include "ndsTaskManager.h"
#include "ndsThreadTask.h"

#include "sis8300Device.h"
#include "sis8300AIChannelGroup.h"
#include "sis8300AIChannel.h"

#include "sis8300drv.h"


std::string sis8300AIChannelGroup::PV_REASON_PERFTIMING = "PerfTiming";


/**
 * @brief AI ChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation.
 */
sis8300AIChannelGroup::sis8300AIChannelGroup(const std::string& name) :
        nds::ChannelGroup(name) {

    registerOnRequestStateHandler(nds::CHANNEL_STATE_DISABLED, nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&sis8300AIChannelGroup::onSwitchProcessing, this, _1, _2));

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&sis8300AIChannelGroup::onLeaveProcessing, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
            boost::bind(&sis8300AIChannelGroup::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&sis8300AIChannelGroup::onEnterError, this, _1, _2));

    _DaqTask = nds::ThreadTask::create(nds::TaskManager::generateName("daqTask"),
            epicsThreadGetStackSize(epicsThreadStackSmall),
            epicsThreadPriorityHigh,
            boost::bind(&sis8300AIChannelGroup::daqTask, this, _1));

}


sis8300AIChannelGroup::~sis8300AIChannelGroup() {}


ndsStatus sis8300AIChannelGroup::registerHandlers(nds::PVContainers* pvContainers) {

    nds::ChannelGroup::registerHandlers(pvContainers);

    NDS_PV_REGISTER_FLOAT64(
            sis8300AIChannelGroup::PV_REASON_PERFTIMING,
            &sis8300AIChannelGroup::setFloat64,
            &sis8300AIChannelGroup::getFloat64,
            &_interruptIdPerfTiming);

    return ndsSuccess;
}


/**
 * @brief Set data acquisition clock source.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Data acquisition clock source to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the source of the clock for data acquisition. Valid values are enumerated
 * in #sis8300drv_clk_src from sis8300drv.h.
 */
ndsStatus sis8300AIChannelGroup::setClockSource(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < (epicsInt32)clk_src_internal || value > (epicsInt32)clk_src_rtm01) {
        NDS_ERR("Invalid clock source.");
        return ndsError;
    }

    NDS_DBG("Setting clock source to %d.", value);

    _ClockSource = value;
    _ClockSourceChanged = 1;

    return commitParameters();
}


/**
 * @brief Set sampling clock frequency.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Clock frequency to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the frequency of the clock used for data acquisition when internal
 * clock source is selected. In case of an invalid choice the value is rounded to
 * the nearest lower acceptable frequency.
 */
ndsStatus sis8300AIChannelGroup::setClockFrequency(asynUser* pasynUser, epicsFloat64 value) {
    ndsStatus           status, statusCommit;
    sis8300drv_clk_div  clkDiv;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;
    
    /* Protect against non-positive frequency setting by treating it as a 
     * very low frequency. Meaning it will be rounded up to the lowest supported. */
    if (value < 1) {
        value = 1;
    }
    
    /* Calculate desired clock divisor. */
    clkDiv = (sis8300drv_clk_div)(SIS8300DRV_INT_CLK_FREQ / value);
    
    /* Verify that clock divider is not higher than maximum allowed*/
    if (clkDiv > SIS8300DRV_CLKDIV_MAX) {
        clkDiv = SIS8300DRV_CLKDIV_MAX;
    }
    
    /* Verify that clock divider is not lower than minimum allowed*/
    if (clkDiv < SIS8300DRV_CLKDIV_INT_MIN) {
        clkDiv = SIS8300DRV_CLKDIV_INT_MIN;
    }

    _ClockFrequency = (epicsFloat64)SIS8300DRV_INT_CLK_FREQ / (epicsFloat64)clkDiv;
    _ClockFrequencyChanged = 1;

    /* Compare floored values since this will otherwise produce errors with
     * larger clock divisors. */
    if (floor(_ClockFrequency) != floor(value)) {
        NDS_ERR("Invalid clock frequency.");
        status = ndsError;
    }

    NDS_DBG("Setting clock frequency to %f.", _ClockFrequency);

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        status = ndsError;
    }
    return status;
}


/**
 * @brief Set clock divisor.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Clock divisor to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the divisor for the clock used for data acquisition when external
 * clock source is selected. This determines the sampling clock frequency in
 * case of external clock source. Valid values are enumerated in
 * #sis8300drv_clk_div from sis8300drv.h. In case of an invalid choice
 * the value is rounded to the nearest higher acceptable clock divisor.
 */
ndsStatus sis8300AIChannelGroup::setClockDivisor(asynUser* pasynUser, epicsInt32 value) {
    ndsStatus           status, statusCommit;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;

    if (value < SIS8300DRV_CLKDIV_MIN) {
        NDS_ERR("Clock divisor %d too low.", value);
        value = (epicsInt32)SIS8300DRV_CLKDIV_MIN;
        status = ndsError;
    }

    if (value > SIS8300DRV_CLKDIV_MAX) {
        NDS_ERR("Clock divisor %d too high.", value);
        value = (epicsInt32)SIS8300DRV_CLKDIV_MAX;
        status = ndsError;
    }

    NDS_DBG("Setting clock divisor to %d.", value);

    _ClockDivisor = value;
    _ClockDivisorChanged = 1;

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        status = ndsError;
    }
    return status;
}


/**
 * @brief Set number of samples to acquire for each channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Number of samples to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the number of samples to acquire for each enabled channel. The number of
 * samples cannot be negative or smaller than the absolute value of trigger delay.
 * The desired number of samples value is checked to fit into device memory
 * taking the number of currently enabled channels into account. In case of
 * error the value is rounded to the nearest acceptable value.
 */
ndsStatus sis8300AIChannelGroup::setSamplesCount(asynUser *pasynUser, epicsInt32 value) {
    ndsStatus   status, statusSamplesCount, statusCommit;
    epicsInt32  samplesCountMax;

    NDS_TRC("%s", __func__);
	NDS_DBG("Requested sample number = %d", value);
	// Reads from RAM should be aligned by offsets of 32.
	value = SIS8300NDS_ROUNDUP_TWOHEX(value);	
	NDS_DBG("Requested sample number rounded up to %d for memory alignment reasons.", value);
    status = ndsSuccess;

    if (value < 0) {
        NDS_ERR("Number of samples cannot be negative.");
        value = 0;
        status = ndsError;
    }
    
    /* Only negative trigger delays are supported (samples before trigger). */
    if (value < -_TriggerDelay) {
        NDS_ERR("Number of samples cannot be less then absolute value of trigger delay %d.", -_TriggerDelay);
        value = -_TriggerDelay;
        status = ndsError;
    }

    /* If the device is in a state with an open device descriptor check
     * if the setting is valid with respect to available device memory.
     * Check this with the current number of enabled channels.
     * If the desired number of samples is to high the value is clipped. */
    if (_device->getCurrentState() != nds::DEVICE_STATE_OFF &&
            _device->getCurrentState() != nds::DEVICE_STATE_IOC_INIT) {
        statusSamplesCount = checkSamplesConfig(value, -1, &samplesCountMax);
        if (statusSamplesCount != ndsSuccess) {
            value = samplesCountMax;
            status = ndsError;
        }
    }

    NDS_DBG("Setting number of samples to %d.", value);

    _SamplesCount = value;
    _SamplesCountChanged = 1;

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        status = ndsError;
    }
    return status;
}

/**
 * @brief Trigger an acquisition.
 * @param [in] pasynUser Asyn user struct.
 *
 * @retval ndsSuccess Acquisition started successfully.
 * @retval ndsError Failed to start acquisition.
 *
 * The value parameter is ignored, the flag that signifies that the acquisition
 * was triggered by the user is set and the channel group is put into PROCESSING
 * state.
 *
 * Starting an acquisition is only allowed when the Device is in ON state and
 * the channel group is in DISABLED state.
 */
ndsStatus sis8300AIChannelGroup::setTrigger(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (_device->getCurrentState() != nds::DEVICE_STATE_ON) {
        NDS_ERR("Acquisition can only be started when device is in ON state.");
        return ndsError;
    }

    if (getCurrentState() != nds::CHANNEL_STATE_DISABLED) {
        NDS_ERR("Acquisition can only be started when channel group is in DISABLED state.");
        return ndsError;
    }

    _isTriggered = 1;
    return start();
}


/**
 * @brief Parse the trigger condition string.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] data Trigger condition string.
 * @param [in] numchars Number of characters in the trigger condition string.
 * @param [out] nbytesTransferead Number of characters from the
 * trigger condition string that have been processed.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Error while parsing the trigger condition string or parsing
 * the condition vector.
 */
ndsStatus sis8300AIChannelGroup::setTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead) {
    nds::Trigger    *trigger;
    std::string     triggerString(data);

    NDS_TRC("%s", __func__);

    trigger = new nds::Trigger(_device);
    *nbytesTransferead = numchars;

    if (trigger->parseTriggerCondition(data) == ndsSuccess) {
        if (onTriggerConditionParsed(pasynUser, *trigger) == ndsSuccess) {
            if (_trigger) {
                delete _trigger;
            }
            _trigger = trigger;
            _TriggerConditionString = triggerString;
            _TriggerConditionChanged = 1;
            return commitParameters();;
        } else {
            NDS_ERR("Trigger's condition handler error.");
        }
    } else {
        NDS_ERR("Trigger's condition parsing error.");
    }

    return ndsError;
}


/**
 * @brief Validate the parsed contents of the trigger condition vector.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] trigger Parsed trigger object.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Invalid trigger specification.
 *
 * The card supports only one trigger channel at a time. Supported trigger
 * channels are "frontpanel[0-3]" and "backplane[0-7]" with either rising
 * or falling edge.
 */
ndsStatus sis8300AIChannelGroup::onTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger) {
    nds::Channel        *ch;
    nds::ChannelGroup   *cg;
    Operations          operation;

    NDS_TRC("%s", __func__);

    if (trigger.condition.size() != 1) {
        NDS_ERR("Only one trigger condition at once supported.");
        return ndsError;
    }

    for(nds::ConditionVector::const_iterator iter = trigger.condition.begin();
            iter != trigger.condition.end(); ++iter) {
        ch = (*iter)->getChannel();
        cg = ch->getChannelGroup();
        operation = (*iter)->getOperation();
        if (cg->getName() != SIS8300NDS_FPCG_NAME && cg->getName() != SIS8300NDS_BPCG_NAME) {
            NDS_ERR("Only front panel and backplane trigger lines supported.");
            return ndsError;
        }
        if (operation != NDS_TC_OP_NONE
                && operation != NDS_TC_OP_RISING
                && operation != NDS_TC_OP_FALLING) {
            NDS_ERR("Trigger operation %d not supported.", operation);
            return ndsError;
        }
        if ((*iter)->isNegated()) {
            NDS_ERR("Negated trigger condition not supported.");
            return ndsError;
        }
        if ((*iter)->isMinus()) {
            NDS_ERR("Negative trigger condition not supported.");
            return ndsError;
        }
    }

    _TriggerConditionChanged = 1;

    return ndsSuccess;
}


/**
 * @brief Set trigger delay.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Trigger delay in number of samples.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Trigger delay determines the number of samples to be acquired before the
 * actual external trigger. Only negative values are supported. The absolute
 * value of trigger delay cannot be larger than the number of samples. In case of
 * error the value is rounded to the nearest acceptable value.
 */
ndsStatus sis8300AIChannelGroup::setTriggerDelay(asynUser *pasynUser, epicsInt32 value) {
    ndsStatus   status, statusCommit;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;

    if (value > 0) {
        NDS_ERR("Positive trigger delay not supported.");
        value = 0;
        status = ndsError;
    }

    if (value < -SIS8300DRV_MAX_PRETRIG) {
        NDS_ERR("Trigger delay smaller than %d not supported.", -SIS8300DRV_MAX_PRETRIG);
        value = -SIS8300DRV_MAX_PRETRIG;
        status = ndsError;
    }

    if (value < -_SamplesCount) {
        NDS_ERR("Number of samples cannot be larger then absolute value of trigger delay.");
        value = -_SamplesCount;
        status = ndsError;
    }

    NDS_DBG("Setting trigger delay to %d.", value);

    _TriggerDelay = value;
    _TriggerDelayChanged = 1;

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        status = ndsError;
    }
    return status;
}


/**
 * @brief Set number of extra triggers the card will accept in a single acquisition.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Number of re-triggers.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Negative values mean infinite successive triggers are accepted which means.
 * that the driver will automatically start waiting for another external
 * trigger after acquisition is finished and data is read from the device.
 */
ndsStatus sis8300AIChannelGroup::setTriggerRepeat(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    NDS_DBG("Setting trigger repeat to %d.", value);

    _TriggerRepeat = value;
    _TriggerRepeatChanged = 1;

    return commitParameters();
}

/**
 * @brief State handler for transition from DISABLED to PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsBlockTransition No enabled channels or zero number of samples.
 * @retval ndsError Invalid acquisition parameters.
 *
 * The device is armed which either means it starts waiting for an external
 * trigger or starts an acquisition immediately (in case the acquisition
 * was triggered by the user). The only parameter that is committed to hardware
 * is the selection of either external or software trigger. If the number of
 * samples or the number of enabled channels is zero the transition is blocked.
 */
ndsStatus sis8300AIChannelGroup::onSwitchProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    int                 status;
    sis8300drv_trg_src  trgSrc;

    NDS_TRC("%s", __func__);

    trgSrc = trg_src_soft;

    /* If there are no samples to acquire there will be nothing to do and we
     * might as well stay disabled. */
    if (!_SamplesCount) {
        NDS_ERR("Number of samples is zero, doing nothing.");
        return ndsBlockTransition;
    }

    /* If there are no samples to acquire there will be nothing to do and we
     * might as well stay disabled. */
    if (!getEnabledChannels()) {
        NDS_ERR("No enabled channels, doing nothing.");
        return ndsBlockTransition;
    }

    if (_isTriggered) {
        trgSrc = trg_src_soft;
    } else {
        trgSrc = trg_src_external;
    }

    NDS_DBG("Calling sis8300drv_set_trigger_source with %u", trgSrc);
    status = sis8300drv_set_trigger_source(_DeviceUser, trgSrc);
    SIS8300NDS_STATUS_CHECK("sis8300drv_set_trigger_source", status);

    /* Start data acquisition and task that will wait for finish. */
    NDS_DBG("Calling sis8300drv_arm_device");
    status = sis8300drv_arm_device(_DeviceUser);
    SIS8300NDS_STATUS_CHECK("sis8300drv_arm_device", status);

    _DaqFinished = 0;
    _DaqTask->start();

    return ndsSuccess;
}


/**
 * @brief State handler for transition from PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Disarm the device.
 */
ndsStatus sis8300AIChannelGroup::onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    NDS_DBG("DaqTask is in state %s.", _DaqTask->getStateStr().c_str());
    sis8300drv_disarm_device(_DeviceUser);

    _PerfTiming = timemsec();

    return ndsSuccess;
}


/**
 * @brief State handler for transition to DISABLED.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Commit all parameters to hardware. If the trigger repeat is set to infinite
 * then immediately restart the acquisition. Also set the default value for
 * trigger condition since it is a waveform record and can't have the default
 * value specified in the database.
 */
ndsStatus sis8300AIChannelGroup::onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to) {
    ndsStatus           statusCommit;
    size_t              nbytesTransferead;
    sis8300AIChannel    *ch;
    int                 status;
    unsigned            channel_mask;

    NDS_TRC("%s", __func__);

    _PerfTiming = timemsec() - _PerfTiming;
    doCallbacksFloat64(_PerfTiming, _interruptIdPerfTiming);

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        return ndsError;
    }

    /* Channels that were not enabled before acquisition started will not
     * transition to DISABLED automatically and will not commit their parameters. */
    if (from == nds::CHANNEL_STATE_PROCESSING) {

        status = sis8300drv_get_channel_mask(_DeviceUser, &channel_mask);
        SIS8300NDS_STATUS_CHECK("sis8300drv_get_channel_mask", status);

        for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
            ch = dynamic_cast<sis8300AIChannel *>(iter->second);
            if (!((1 << ch->getChannelNumber()) & channel_mask)) {
                NDS_DBG("Channel %d commit.", iter->second->getChannelNumber());
                statusCommit = ch->commitParameters();
                if (statusCommit != ndsSuccess) {
                    NDS_ERR("AI channel failed to commit parameters.");
                    SIS8300NDS_MSGERR("AI channel failed to commit parameters.");
                    return ndsError;
                }

            }
        }

    }

    /* If we came from PROCESSING and trigger repeat is set
     * and the acquisition was not soft triggered then go back to processing. */
    if (from == nds::CHANNEL_STATE_PROCESSING &&
            _TriggerRepeat < 0 &&
            !_isTriggered) {
        start();
    }

    if (from == nds::CHANNEL_STATE_PROCESSING &&
            _TriggerRepeatRemaining &&
            !_isTriggered) {
        _TriggerRepeatRemaining--;
        start();
    }

    /* If we came from IOC_INITIALIZATION update the trigger condition readback.
     * This default has to be set here since one cannot set default values of
     * waveform records. */
    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        /* Set default external trigger line - frontpanel0. */
        setTriggerCondition(NULL, "frontpanel0", 12, &nbytesTransferead);
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the device to ERROR state.
 */
ndsStatus sis8300AIChannelGroup::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _device->error();

    return ndsSuccess;
}


/**
 * @brief Message handler for START message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always START in our case).
 *
 * @retval ndsSuccess Acquisition started successfully.
 * @retval ndsError Failed to start acquisition.
 *
 * Starting an acquisition is only allowed when the Device is in ON state and
 * the channel group is in DISABLED state.
 */
ndsStatus sis8300AIChannelGroup::handleStartMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_TRC("%s", __func__);

    if (_device->getCurrentState() != nds::DEVICE_STATE_ON) {
        NDS_ERR("Acquisition can only be started when device is in ON state.");
        return ndsError;
    }

    if (getCurrentState() != nds::CHANNEL_STATE_DISABLED) {
        NDS_ERR("Acquisition can only be started when channel group is in DISABLED state.");
        return ndsError;
    }

    _isTriggered = 0;
    _TriggerRepeatRemaining = _TriggerRepeat - 1;
    return start();
}


/**
 * @brief Message handler for STOP message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always STOP in our case).
 *
 * @retval ndsSuccess Acquisition started successfully.
 * @retval ndsError Failed to stop acquisition.
 *
 * Starting an acquisition is only allowed when the channel group is in
 * PROCESSING state.
 */
ndsStatus sis8300AIChannelGroup::handleStopMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_TRC("%s", __func__);

    if (getCurrentState() != nds::CHANNEL_STATE_PROCESSING) {
        NDS_ERR("Acquisition can only be stopped when in PROCESSING state.");
        return ndsError;
    }

    /* This is a hack to allow stopping a series of acquisitions where infinite
     * trigger repeat is set. */
    _isTriggered = 1;
    _TriggerRepeatRemaining = 0;
    return stop();
}


/**
 * @brief Set device context for the channel group.
 * @param [in] newDeviceUser Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context and push the device context to all the channels.
 */
ndsStatus sis8300AIChannelGroup::setDeviceUser(sis8300drv_usr *newDeviceUser) {
    _DeviceUser = newDeviceUser;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        (dynamic_cast<sis8300AIChannel *>(iter->second))->setDeviceUser(newDeviceUser);
    }
    return ndsSuccess;
}


/**
 * @brief Commit parameters that gave changed to hardware.
 *
 * @retval ndsSuccess Parameters committed successfully or writing to hardware
 * was not possible at this time.
 * @retval ndsError Error while trying to write parameters to hardware.
 *
 * For each parameter that has changed since this function was called last
 * the appropriate board settings are set. Where appropriate the AI channels
 * parameters are updated also. Also for each parameter that is actually written
 * to hardware the appropriate readback is updated. This results in readbacks
 * being updated at times when the appropriate settings have been actually written
 * to hardware.
 *
 * This function does nothing if called when the AI channel group is in PROCESSING
 * state or if the device is not in a state with an open file descriptor.
 * If there is an error while writing parameters to hardware the channel group
 * is put into ERROR state.
 */
ndsStatus sis8300AIChannelGroup::commitParameters() {
    int                 status;
    sis8300drv_trg_ext  trgExt;
    sis8300drv_clk_src  clkSrc;
    sis8300drv_clk_div  clkDiv;
    unsigned            nsamples, npretrig, trgMask, edgeMask;
    nds::Channel        *ch;

    NDS_TRC("%s", __func__);

    clkSrc = (sis8300drv_clk_src)_ClockSource;
    clkDiv = (sis8300drv_clk_div)_ClockDivisor;
    trgExt = trg_ext_harlink;
    npretrig = (unsigned)(-_TriggerDelay);
    trgMask = 0;
    edgeMask = 0;

    if (getCurrentState() != nds::CHANNEL_STATE_DISABLED ||
            _device->getCurrentState() == nds::DEVICE_STATE_OFF ||
            _device->getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        NDS_DBG("AI channel group not committing parameters.");
        return ndsSuccess;
    }

    /* If any of the clock parameters changed all the board clocking settings
     * have to be updated since they are interdependent. */
    if (_ClockSourceChanged || _ClockFrequencyChanged || _ClockDivisorChanged) {
        /* For internal clock source calculate clock divider from frequency.
         * For external clock source just use clock divisor setting directly. */
        if (clkSrc == clk_src_internal) {
            clkDiv = (sis8300drv_clk_div)floor(SIS8300DRV_INT_CLK_FREQ / _ClockFrequency);
        }

        NDS_DBG("Calling sis8300drv_set_clock_source with %u", clkSrc);
        status = sis8300drv_set_clock_source(_DeviceUser, clkSrc);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_clock_source", status);

        NDS_DBG("Calling sis8300drv_set_clock_divider with %u", clkDiv);
        status = sis8300drv_set_clock_divider(_DeviceUser, clkDiv);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_clock_divider", status);
    }

    if (_ClockSourceChanged) {
        doCallbacksInt32(_ClockSource, _interruptIdClockSource);
        _ClockSourceChanged = 0;
    }

    if (_ClockFrequencyChanged) {
        doCallbacksFloat64(_ClockFrequency, _interruptIdClockFrequency);
        _ClockFrequencyChanged = 0;
    }

    if (_ClockDivisorChanged) {
        doCallbacksInt32(_ClockDivisor, _interruptIdClockDivisor);
        _ClockDivisorChanged = 0;
    }

    if (_SamplesCountChanged) {
        /* Round number of samples up to next multiple of 32, board wants this. */
        nsamples = SIS8300NDS_ROUNDUP_TWOHEX(_SamplesCount);

        NDS_DBG("Calling sis8300drv_set_nsamples %u", nsamples);
        status = sis8300drv_set_nsamples(_DeviceUser, nsamples);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_nsamples", status);

        /* Set channels _SamplesCount so they know how much data to put into their buffers. */
        for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
            iter->second->setSamplesCount(NULL, nsamples);
        }

        doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
        _SamplesCountChanged = 0;
    }

    if (_TriggerConditionChanged) {
        /* Clear existing harlink external trigger settings. */
        NDS_DBG("Calling sis8300drv_set_external_setup with %u %u %u", trg_ext_harlink, 0, 0);
        status = sis8300drv_set_external_setup(_DeviceUser, trg_ext_harlink, 0, 0);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_external_setup", status);

        /* Clear existing mlvds external trigger settings. */
        NDS_DBG("Calling sis8300drv_set_external_setup with %u %u %u", trg_ext_mlvds, 0, 0);
        status = sis8300drv_set_external_setup(_DeviceUser, trg_ext_mlvds, 0, 0);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_external_setup", status);

        /* Construct trigger channel and edge mask and determine external
         * trigger setup. */
        ch = _trigger->condition.front()->getChannel();
        if (ch->getChannelGroup()->getName() == SIS8300NDS_FPCG_NAME) {
            trgExt = trg_ext_harlink;
        } else if (ch->getChannelGroup()->getName() == SIS8300NDS_BPCG_NAME) {
            trgExt = trg_ext_mlvds;
        }
        trgMask = 1 << ch->getChannelNumber();
        if (_trigger->condition.front()->getOperation() == NDS_TC_OP_FALLING) {
            edgeMask = 1 << ch->getChannelNumber();
        }

        NDS_DBG("Calling sis8300drv_set_external_setup with %u %u %u", trgExt, trgMask, edgeMask);
        status = sis8300drv_set_external_setup(_DeviceUser, trgExt, trgMask, edgeMask);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_external_setup", status);

        doCallbacksOctetStr(_TriggerConditionString, ASYN_EOM_END, _interruptIdTriggerCondition);
        _TriggerConditionChanged = 0;
    }

    if (_TriggerDelayChanged) {
        NDS_DBG("Calling sis8300drv_set_npretrig %u", npretrig);
        status = sis8300drv_set_npretrig(_DeviceUser, npretrig);
        SIS8300NDS_STATUS_ASSERT("sis8300drv_set_npretrig", status);

        doCallbacksInt32(_TriggerDelay, _interruptIdTriggerDelay);
        _TriggerDelayChanged = 0;
    }

    if (_TriggerRepeatChanged) {
        doCallbacksInt32(_TriggerRepeat, _interruptIdTriggerRepeat);
        _TriggerRepeatChanged = 0;
    }

    return ndsSuccess;
}


/**
 * @brief Flag all parameters as changed.
 *
 * @retval ndsSuccess All parameters flagged as changed.
 *
 * All parameters of the AI channel group and all parameters of all
 * AI channels are flagged as changed.
 */
ndsStatus sis8300AIChannelGroup::markAllParametersChanged() {
    NDS_TRC("%s", __func__);

    _ClockSourceChanged = 1;
    _ClockFrequencyChanged = 1;
    _ClockDivisorChanged = 1;
    _SamplesCountChanged = 1;
    _TriggerConditionChanged = 1;
    _TriggerDelayChanged = 1;
    _TriggerRepeatChanged = 1;
    NDS_INF("markAllParametersChanged for each channel");
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        dynamic_cast<sis8300AIChannel *>(iter->second)->markAllParametersChanged();
    }

    return ndsSuccess;
}


/**
 * @brief Commit parameters on all channels.
 *
 * @retval ndsSuccess All channels committed parameters.
 * @retval ndsError Commit parameters failed on one of the channels.
 */
ndsStatus sis8300AIChannelGroup::commitAllChannels() {
    ndsStatus status, statusCombined;

    NDS_TRC("%s", __func__);

    statusCombined = ndsSuccess;

    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        status = dynamic_cast<sis8300AIChannel *>(iter->second)->commitParameters();
        if (status != ndsSuccess) {
            statusCombined = ndsError;
        }
    }

    return statusCombined;
}


/**
 * @brief Reset attenuators on all channels.
 *
 * @retval ndsSuccess All channels attenuators reset.
 * @retval ndsError Resetting attenuatots failed on one of the channels.
 */
ndsStatus sis8300AIChannelGroup::resetAllAttenuators() {
    ndsStatus           status, statusCombined;

    NDS_TRC("%s", __func__);

    statusCombined = ndsSuccess;

    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        status = dynamic_cast<sis8300AIChannel *>(iter->second)->resetAttenuation();
        if (status != ndsSuccess) {
            statusCombined = ndsError;
        }
    }
    
    return statusCombined;
}


/**
 * @brief Check if the combination of number of samples and enabled channels is valid.
 * @param [in] samplesCount Number of samples to check.
 * @param [in] nchannels Number of enabled channels to check.
 * @param [out] samplesCountMax Maximum number of samples for the specified
 * number of channels enabled.
 *
 * @retval ndsSuccess Configuration valid.
 * @retval ndsSuccess Configuration not valid.
 *
 * Read the amount of available board memory and check if the desired configuration
 * (number of samples and number of enabled channels) is valid. If the #samplesCount
 * argument is negative then the currently set number of samples is taken into
 * account. if the #nchannels argument is negative then the current number of
 * enabled channels is taken into account. If the #nchannels argument is zero then
 * the number of enabled channels is taken to be one when performing the check.
 * If the #samplesCountMax is not NULL then it is set to the maximum number
 * of samples for the provided number of enabled channels.
 */
ndsStatus sis8300AIChannelGroup::checkSamplesConfig(epicsInt32 samplesCount, epicsInt32 nchannels, epicsInt32 *samplesCountMax) {
    ndsStatus   status;
    int         statusLib;
    unsigned    space;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;
    if (samplesCount < 0) {
        samplesCount = _SamplesCount;
    }

    if (nchannels < 0) {
        nchannels = getEnabledChannels();
    }

    if (nchannels == 0) {
        nchannels = 1;
    }

    statusLib = sis8300drv_get_space(_DeviceUser, &space);
    SIS8300NDS_STATUS_CHECK("sis8300drv_get_space", statusLib);
    NDS_DBG("sis8300drv_get_space returned %u.", space);

    /* Round number of samples up to next multiple of 32, board wants this. */
    if (nchannels * SIS8300NDS_ROUNDUP_TWOHEX(samplesCount) > space) {
        NDS_ERR("Number of samples %d too large for number of enabled channels %d.", samplesCount, nchannels);
        status = ndsError;
    }

    if (samplesCountMax) {
        *samplesCountMax = (epicsInt32)SIS8300NDS_ROUNDDOWN_TWOHEX(space / nchannels);
    }
    return status;
}


/**
 * @brief Get number of currently enabled channels.
 *
 * @retval nchannels Number of currently enabled channels.
 */
int sis8300AIChannelGroup::getEnabledChannels() {
    int nchannels;

    nchannels = 0;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        if (iter->second->isEnabled()) {
            nchannels++;
        }
    }

    return nchannels;
}


int sis8300AIChannelGroup::isDaqFinished() {
    return _DaqFinished;
}


epicsFloat64 sis8300AIChannelGroup::timemsec() {
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (double)ts.tv_sec * 1.0e3 + ts.tv_nsec / 1.0e6;
}


ndsStatus sis8300AIChannelGroup::daqTask(nds::TaskServiceBase &service) {
    int status;

    NDS_TRC("%s", __func__);

    NDS_DBG("Calling sis8300drv_wait_acq_end.");
    status = sis8300drv_wait_acq_end(_DeviceUser);
    lock();
    if (status != status_success) {
        NDS_ERR("sis8300drv_wait_acq_end error: %d.", status);
        _DaqTask->setState(nds::ndsThreadStopped);
        error();
        unlock();
        return ndsError;
    }

    if (getCurrentState() != nds::CHANNEL_STATE_PROCESSING) {
        NDS_DBG("Data acquisition canceled.");
        unlock();
        return ndsSuccess;
    }

    NDS_DBG("Data acquisition finished.");
    _DaqFinished = 1;
    _DaqTask->setState(nds::ndsThreadStopped);
    stop();
    unlock();

    return ndsSuccess;
}
