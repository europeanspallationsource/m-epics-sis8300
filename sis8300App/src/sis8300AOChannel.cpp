/**
 * Struck 8300 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300AOChannel.cpp
 * @brief Implementation of sis8300 card analog output channel in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "ndsADIOChannel.h"
#include "ndsChannelStateMachine.h"
#include "ndsChannelStates.h"

#include "sis8300Device.h"
#include "sis8300AOChannelGroup.h"
#include "sis8300AOChannel.h"

#include "sis8300drv.h"


/**
 * @brief AO channel constructor.
 */
sis8300AOChannel::sis8300AOChannel() {

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&sis8300AOChannel::onEnterError, this, _1, _2));
}


sis8300AOChannel::~sis8300AOChannel() {}


ndsStatus sis8300AOChannel::setValueFloat64(asynUser* pasynUser, epicsFloat64 value) {
    int status;

    NDS_TRC("%s", __func__);

    /* If we don't have the device context we cannot access the device. */
    if (!_DeviceUser) {
        NDS_ERR("No device context, device is probably not in ON state.");
        return ndsError;
    }

    status = sis8300drv_write_ao(_DeviceUser, getChannelNumber(), value);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_write_ao", status);

    NDS_DBG("Channel %d wrote value %lfV.", getChannelNumber(), value);
    NDS_INF("sis8300AOChannel::setValueFloat64");
    getValueFloat64(pasynUser, &value);
    doCallbacksFloat64(value, _interruptIdValueFloat64);
    return ndsSuccess;
}


ndsStatus sis8300AOChannel::getValueFloat64(asynUser* pasynUser, epicsFloat64 *value) {
    int status;

    NDS_TRC("%s", __func__);

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    /* If we don't have the device context we cannot access the device. */
    if (!_DeviceUser) {
        NDS_ERR("No device context, device is probably not in ON state.");
        return ndsError;
    }

    status = sis8300drv_read_ao(_DeviceUser, getChannelNumber(), value);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_read_ao", status);

    NDS_DBG("Channel %d read value %lfV.", getChannelNumber(), *value);

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the AO channel group to ERROR state.
 */
ndsStatus sis8300AOChannel::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    sis8300AOChannelGroup   *cg;

    NDS_TRC("%s", __func__);

    cg = dynamic_cast<sis8300AOChannelGroup *>(getChannelGroup());

    cg->lock();
    cg->error();
    cg->unlock();

    return ndsSuccess;
}


/**
 * @brief Set device context for the channel.
 * @param [in] newDeviceUser Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context.
 */
ndsStatus sis8300AOChannel::setDeviceUser(sis8300drv_usr *newDeviceUser) {
    _DeviceUser = newDeviceUser;
    return ndsSuccess;
}
