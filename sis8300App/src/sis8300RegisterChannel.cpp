/**
 * Struck 8300 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300RegisterChannel.cpp
 * @brief Implementation of sis8300 card device register access channel class.
 * @author kstrnisa
 * @date 15.11.2014
 */

#include "sis8300Device.h"
#include "sis8300RegisterChannel.h"
#include "sis8300RegisterChannelGroup.h"

#include "sis8300drv.h"


ndsStatus sis8300RegisterChannel::getValueUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask){
    int                 status;
    unsigned            data;
    nds::ChannelGroup   *cg;
    
    NDS_TRC("%s", __func__);
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }
    
    NDS_DBG("Register 0X%X read with mask 0X%X.", getChannelNumber(), mask);

    cg = getChannelGroup();

    cg->lock();
    
    if (_device->getCurrentState() != nds::DEVICE_STATE_ON && 
        _device->getCurrentState() != nds::DEVICE_STATE_INIT) {
        NDS_ERR("Device register can be read only when device is in ON or INIT state.");
        cg->unlock();
        return ndsError;
    }
    
    status = sis8300drv_reg_read(_DeviceUser, (unsigned)getChannelNumber(), &data);
    SIS8300NDS_STATUS_CHECK_UNLOCK("sis8300drv_reg_read", status, cg);
    
    NDS_DBG("Register 0X%X read value: 0X%X.", getChannelNumber(), data);
    
    cg->unlock();
    
    *value = ((epicsUInt32)data & mask);
    
    doCallbacksUInt32Digital(*value, _interruptIdValueUInt32Digital);
    return ndsSuccess;
}


ndsStatus sis8300RegisterChannel::setValueUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask){
    int                 status;
    unsigned            data, ch;
    nds::ChannelGroup   *cg;
    
    NDS_TRC("%s", __func__);
    
    NDS_DBG("Register 0X%X write value 0X%X with mask 0X%X.", getChannelNumber(), value, mask);

    cg = getChannelGroup();
    ch = (unsigned)getChannelNumber();

    cg->lock();
    
    if (_device->getCurrentState() != nds::DEVICE_STATE_ON && 
        _device->getCurrentState() != nds::DEVICE_STATE_INIT) {
        NDS_ERR("Device register can be modified only when device is in ON or INIT state.");
        cg->unlock();
        return ndsError;
    }
    
    status = sis8300drv_reg_read(_DeviceUser, ch, &data);
    SIS8300NDS_STATUS_CHECK_UNLOCK("sis8300drv_reg_read", status, cg);
    
    NDS_DBG("Register 0X%X read value 0X%X.", getChannelNumber(), data);
    
    value = ((epicsUInt32)data & ~mask) | (mask & value);
    
    status = sis8300drv_reg_write(_DeviceUser, ch, (unsigned)value);
    SIS8300NDS_STATUS_CHECK_UNLOCK("sis8300drv_reg_write", status, cg);
    
    NDS_DBG("Register 0X%X writen value 0X%X.", getChannelNumber(), value);
    
    cg->unlock();
    
    doCallbacksUInt32Digital(value, _interruptIdValueUInt32Digital);
    return ndsSuccess;
}


/**
 * @brief Set device context for the channel.
 * @param [in] newDeviceUser Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context.
 */
ndsStatus sis8300RegisterChannel::setDeviceUser(sis8300drv_usr *newDeviceUser) {
    NDS_DBG("Setting device context for register channel %d.", getChannelNumber());
    _DeviceUser = newDeviceUser;
    return ndsSuccess;
}
