/**
 * @file sis8300RegisterChannel.h
 * @brief Header file defining the sis8300 card device register access channel class.
 * @author kstrnisa
 * @date 15.11.2014
 */

#ifndef _sis8300RegisterChannel_h
#define _sis8300RegisterChannel_h

#include "ndsChannel.h"

#include "sis8300drv.h"

/**
 * @brief sis8300 specific nds::Channel class that supports device register access channels.
 */
class sis8300RegisterChannel : public nds::Channel {

public:
    /* Parameters implemented from base class(es). */
    ndsStatus getValueUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask);
    ndsStatus setValueUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask);

    ndsStatus setDeviceUser(sis8300drv_usr *newDeviceUser);

protected:
    sis8300drv_usr  *_DeviceUser;   /**< User context for sis8300drv, set by Device. */
};

#endif /* _sis8300RegisterChannel_h */
