/**
 * @file sis8300RegisterChannelGroup.h
 * @brief Header file defining the sis8300 card device register access channel group class.
 * @author kstrnisa
 * @date 15.11.2014
 */

#ifndef _sis8300RegisterChannelGroup_h
#define _sis8300RegisterChannelGroup_h

#include "ndsAutoChannelGroup.h"

#include "sis8300drv.h"


/**
 * @brief sis8300 specific nds::ChannelGroup class that supports device register access channels.
 */
class sis8300RegisterChannelGroup : public nds::AutoChannelGroup {

public:
    sis8300RegisterChannelGroup(const std::string& name);
    
    ndsStatus readAllChannels();
    ndsStatus setDeviceUser(sis8300drv_usr *newDeviceUser);

protected:
    sis8300drv_usr  *_DeviceUser;   /**< User context for sis8300drv, set by NDS Device. */
};


#endif /* _sis8300RegisterChannelGroup_h */
