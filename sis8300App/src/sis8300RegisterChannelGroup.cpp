/**
 * Struck 8300 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300RegisterChannelGroup.cpp
 * @brief Implementation of sis8300 card device register access channel group class.
 * @author kstrnisa
 * @date 15.11.2014
 */

#include "ndsChannel.h"
#include "ndsAutoChannelGroup.h"

#include "sis8300RegisterChannel.h"
#include "sis8300RegisterChannelGroup.h"

#include "sis8300drv.h"


nds::Channel *createRegisterChannel() {
    return new sis8300RegisterChannel();
}


sis8300RegisterChannelGroup::sis8300RegisterChannelGroup(const std::string& name) :
        nds::AutoChannelGroup(name, &createRegisterChannel) {}


/**
 * @brief Read the values of all registered channels (representing device registers).
 *
 * @retval ndsSuccess Always returns success.
 */
ndsStatus sis8300RegisterChannelGroup::readAllChannels() {
    epicsUInt32 value;
    
    NDS_TRC("%s", __func__);
    
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        (dynamic_cast<sis8300RegisterChannel *>(iter->second))->getValueUInt32Digital(NULL, &value, 0xFFFFFFFF);;
    }
    return ndsSuccess;
}


/**
 * @brief Set device context for the channel group.
 * @param [in] newDeviceUser Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context and push the device context to all the channels.
 */
ndsStatus sis8300RegisterChannelGroup::setDeviceUser(sis8300drv_usr *newDeviceUser) {
    _DeviceUser = newDeviceUser;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        (dynamic_cast<sis8300RegisterChannel *>(iter->second))->setDeviceUser(newDeviceUser);
    }
    return ndsSuccess;
}

